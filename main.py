import pandas as pd
import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

def normalize(data):#calculate average and std deviation to normalize data
	data=np.transpose(data)
	average=[np.average(lst) for lst in data]
	print('average:',average)
	stdDev=[np.std(lst) for lst in data]
	print('standard deviation:',stdDev)
	newData=[(lst-average[i])/stdDev[i] for i,lst in enumerate(data)]
	return np.transpose(newData),average,stdDev

def evalError(sess,_input,auto,data,average,stdDev):
	tData=np.transpose(data)
	input=np.transpose([(lst-average[i])/stdDev[i] for i,lst in enumerate(tData)])
	res=np.transpose(sess.run(auto,feed_dict={_input:input}))
	res=[(lst*stdDev[i]+average[i]) for i,lst in enumerate(res)]
	averageError=[np.average([abs(r-d)/r for r,d in zip(resLst,lst)]) for resLst,lst in zip(res,tData)]
	return np.average(averageError)

def buildModel():
	feature=8
	layer=[7,6,5,4,3,2]#encoder's layer
	learningRate=[0.1,0.1,0.1,0.1,0.1,0.1]
	encoderLayerLen=len(layer)
	
	_input=tf.placeholder("float",[None,feature])
	
	autoL=[feature]#generate autoencoder's layer
	autoL.extend([l for l in layer])
	autoL.extend([l for l in layer[-2::-1]])
	autoL.append(feature)
	biases=[tf.Variable(tf.random_uniform([l],-0.01,0.01)) for l in autoL[1:]]#generate a liste of layer's biases
	layerLen=len(biases)
	print(biases)
	weigths=[tf.Variable(tf.random_uniform([lIn,lOut],-0.01,0.01)) for lIn,lOut in zip(autoL[:-1],autoL[1:])]#generate a list of weigths matrix
	print(weigths)
	#generate encoder
	models=[]
	for index,l in enumerate(layer):
		m={}
		prevL=_input
		print('layer',index)
		for i in range(index+1):
			nextL=tf.nn.bias_add(tf.matmul(prevL,weigths[i]),biases[i])
			nextL=tf.nn.tanh(nextL)
			print(nextL,'encode')
			prevL=nextL
		m['encoder']=nextL
		for i in range(index):
			nextL=tf.nn.bias_add(tf.matmul(prevL,weigths[layerLen-index-1+i]),biases[layerLen-index-1+i])
			nextL=tf.nn.tanh(nextL)
			print(nextL,'decode')
			prevL=nextL
		nextL=tf.nn.bias_add(tf.matmul(prevL,weigths[-1]),biases[-1])
		print(nextL,'decode')
		m['autoencoder']=nextL
		m['loss']=tf.reduce_mean(0.5*tf.square(m['autoencoder']-_input))
		#m['error']=tf.reduce_mean(tf.dot(tf.abs(m['autoencoder']-_input),_input))
		print('optimize: ',weigths[index],biases[index],biases[layerLen-1-index])
		m['train']=tf.train.MomentumOptimizer(learningRate[index],momentum=0.6).minimize(m['loss'],var_list=[weigths[index],weigths[layerLen-1-index],biases[index],biases[layerLen-1-index]])
		models.append(m)

	return _input,models


dataset=pd.read_csv('Pokemon.csv')#load csv
rawData=dataset[['Total','HP','Attack','Defense','Sp. Atk','Sp. Def','Speed','Legendary']].as_matrix().astype(float)#selecte feature
data,average,stdDev=normalize(rawData)#normalize data
print(data)
_input,models=buildModel()#build model

#init tensorflow
batch_size=36
batch=[data[i*batch_size:(i+1)*batch_size] for i in range(len(data)//batch_size-1)]
init=tf.global_variables_initializer()

with tf.Session() as sess:
	sess.run(init)
	
	for index,m in enumerate(models):#for each layer of the autoencoder
		print('================ train layer:',index,'================')
		error=sess.run(m['loss'],feed_dict={_input:data})#evaluate error
		while error>0.2:#run training until model converge
			print('error:',error)
			for i in range(1000):#training phase
				sess.run(m['train'],feed_dict={_input:data})
			error=sess.run(m['loss'],feed_dict={_input:data})#evaluate error
		print('final error:',error)
	points=sess.run(models[-1]['encoder'],feed_dict={_input:data})
	points=np.transpose(points)
	plt.scatter(points[0],points[1])
	print('final error: {0:.1f}%'.format(evalError(sess,_input,models[-1]['autoencoder'],rawData,average,stdDev)*100.0))
	plt.show()
